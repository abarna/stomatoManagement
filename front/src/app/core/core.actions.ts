import { Action } from '@ngrx/store';

// APP TITLE & VERSION
export class SetAppTitleAction implements Action{
  readonly type: string = 'SET_APP_TITLE';
  constructor( public payload?: { title: string }){}
}

export class ResetCoreAction implements Action{
  readonly type: string = 'RESET_CORE';
  constructor( public payload?: any ){}
}
