import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { environment } from '@env/environment';

@Injectable()
export class CoreService {
  constructor(
    private titleService: Title
  ) { }

  appTitle: string = environment.appTitle;

  public setAppTitle(routeTitle?: string) {
    const title = this.appTitle + (routeTitle ? ' - ' + routeTitle : '');
    this.titleService.setTitle(title);
  }
}
