import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Action } from '@ngrx/store';
import { Actions, Effect, toPayload } from '@ngrx/effects';

import { CoreService } from '@root/core/core.service';

@Injectable()
export class CoreEffects {
  constructor(
    private actions$: Actions,
    private coreService: CoreService
  ) { }

  @Effect({ dispatch: false })
  public setAppTitle: Observable<Action> = this.actions$
    .ofType('SET_APP_TITLE')
    .map(toPayload)
    .do(payload => {
      this.coreService.setAppTitle(payload.title);
    });
}
