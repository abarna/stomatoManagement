import { Store, Action, createReducer } from 'ngrx-actions';

import { ResetCoreAction, SetAppTitleAction } from '@root/core/core.actions';

export interface ThemeState {
  headerThemes: any;
  changeHeader: any;
  sidenavThemes: any;
  changeSidenav: any;
  headerSelected: any;
  sidenavSelected: any;
  isAuthenticated: boolean;
}

export const initialThemeState: ThemeState = {
  headerThemes: null,
  changeHeader: null,
  sidenavThemes: null,
  changeSidenav: null,
  headerSelected: null,
  sidenavSelected: null,
  isAuthenticated: false
};

export interface InfoState {
  title?: string;
  busy: boolean;
}

export const initialInfoState: InfoState = {
  title: 'Doc App',
  busy: false
};

export interface CoreState {
  info?: InfoState;
  theme?: ThemeState;
}

export const initialCoreState: CoreState = {
  info: initialInfoState,
  theme: initialThemeState
};


@Store(initialCoreState)
export class CoreStore {
  @Action(SetAppTitleAction)
  setAppTitle(state: CoreState, action: SetAppTitleAction) {
    state.info.title = action.payload.title;
  }

  @Action(ResetCoreAction)
  resetCore(state: CoreState, action: ResetCoreAction) {
    state = initialCoreState;
  }
}

export function coreReducer(state, action) {
  return createReducer(CoreStore)(state, action);
}
