import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'doc-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {
  public urlRoute: any = null;
  constructor( public router: Router ) {

  }
  ngOnInit(): void {
    this.urlRoute = this.router.url;
  }

  ngOnDestroy(): void {
  }
}
