import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { environment } from '@env/environment';
import { AuthGuard } from '@root/modules/login/auth.guard';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: environment.defaultRoute,
    pathMatch: 'full'
  },
  {
    path: "404",
    loadChildren: '@root/modules/page-not-found/page-not-found.module#PageNotFoundModule'
  },
  {
    path: 'login',
    loadChildren: '@root/modules/login/login.module#LoginModule'
  },
  {
    path: 'dashboard',
    loadChildren: '@root/modules/dashboard/dashboard.module#DashboardModule',
    // canActivate: [AuthGuard]
  },
  {
    path: 'calendar',
    loadChildren: '@root/modules/calendar/calendar.module#CalendarFeatureModule',
    // canActivate: [AuthGuard]
  },
  {
    path: 'appointments',
    loadChildren: '@root/modules/appointments/appointments.module#AppointmentsModule',
    // canActivate: [AuthGuard]
  },
  {
    path: 'patients',
    loadChildren: '@root/modules/patients/patients.module#PatientsModule',
    // canActivate: [AuthGuard]
  },
  {
    path: 'team',
    loadChildren: '@root/modules/team/team.module#TeamModule',
    // canActivate: [AuthGuard]
  },
  {
    path: 'notes',
    loadChildren: '@root/modules/notes/notes.module#NotesModule',
    // canActivate: [AuthGuard]
  },
  {
    path: 'materials',
    loadChildren: '@root/modules/materials/materials.module#MaterialsModule',
    // canActivate: [AuthGuard]
  },
  {
    path: 'admin',
    loadChildren: '@root/modules/admin/admin.module#AdminModule',
    // canActivate: [AuthGuard]
  },
  {
    path: 'options',
    loadChildren: '@root/modules/options/options.module#OptionsModule',
    // canActivate: [AuthGuard]
  },
  {
    path: '**',
    redirectTo: '/login'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes) //, { enableTracing: true }
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthGuard
  ]
})

export class AppRoutingModule {
}
