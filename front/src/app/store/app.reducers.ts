import { storeLogger } from 'ngrx-store-logger';
import { ActionReducer, MetaReducer, ActionReducerMap } from '@ngrx/store';

import { AppState } from 'app/store/app.state';

import { environment } from 'environments/environment';
import { coreReducer } from '@root/core/core.store';

export const appReducers: ActionReducerMap<AppState> = {
  core: coreReducer
};

export function logger(reducer: ActionReducer<AppState>): any {
  return storeLogger()(reducer);
}

export const metaReducers: MetaReducer<AppState>[] = environment.production ? [] : [logger];
