import { CoreState } from '@root/core/core.store';

export interface AppState {
  core: CoreState;
}
