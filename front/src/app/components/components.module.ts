import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CommonLayoutComponent } from '@root/components/common-layout/common-layout.component';
import { RouterModule } from '@angular/router';
import { Sidebar_Directives } from '@root/shared/directives/side-nav.directive';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { CalendarModule } from "ap-angular2-fullcalendar";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    PerfectScrollbarModule,
    CalendarModule
  ],
  declarations: [
    CommonLayoutComponent,
    Sidebar_Directives,
  ],
  providers: [

  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  exports: [
    CommonLayoutComponent
  ]
})
export class ComponentsModule {}
