import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { CoreService } from '@root/core/core.service';
import { CoreEffects } from '@root/core/core.effects';
import { appReducers, metaReducers } from '@root/store/app.reducers';

import { NgrxActionsModule, NgrxSelect } from 'ngrx-actions';
import { AppState } from '@root/store/app.state';
import { Store } from '@ngrx/store';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { cardPortletDelete, cardPortletRefresh } from '@root/shared/directives/cards.directive';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { AppComponent } from '@root/app.component';
import { AppRoutingModule } from '@root/app.routing';

import { LoginModule } from '@root/modules/login/login.module';
import { ComponentsModule } from '@root/components/components.module';
import { AuthService } from '@root/modules/login/auth.service';
import {
  AuthInterceptor,
  CacheInterceptor,
  CORSInterceptor
} from '@root/modules/login/auth.interceptor';
import { DashboardModule } from '@root/modules/dashboard/dashboard.module';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule,
    AppRoutingModule,
    ComponentsModule,
    LoginModule,
    PerfectScrollbarModule,
    DashboardModule,

    NgbModule.forRoot(),
    StoreModule.forRoot(appReducers, {metaReducers}),
    EffectsModule.forRoot([CoreEffects]),
    NgrxActionsModule
  ],
  declarations: [
    AppComponent,
    cardPortletDelete,
    cardPortletRefresh
  ],
  providers: [
    CoreService,
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CacheInterceptor,
      multi: true
    } ,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CORSInterceptor,
      multi: true
    }
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(ngrxSelect: NgrxSelect, store: Store<AppState>) {
    ngrxSelect.connect(store);
  }
}

