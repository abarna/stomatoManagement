import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { TeamComponent } from '@root/modules/team/team.component';

export const teamRoutes: Routes = [
  {
    path: '',
    component: TeamComponent
  }
];


@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forChild( teamRoutes )]
})
export class TeamRoutingModule {}
