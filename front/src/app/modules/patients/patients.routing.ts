import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { PatientsComponent } from '@root/modules/patients/patients.component';

export const patientsRoutes: Routes = [
  {
    path: '',
    component: PatientsComponent
  }
];


@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forChild( patientsRoutes )]
})
export class PatientsRoutingModule {}
