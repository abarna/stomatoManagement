import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { CalendarComponent } from "@root/modules/calendar/calendar.component";

export const calendarRoutes: Routes = [
  {
    path: '',
    component: CalendarComponent
  }
];


@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forChild( calendarRoutes )]
})
export class CalendarRoutingModule {}
