import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CalendarRoutingModule } from "@root/modules/calendar/calendar.routing";
import { CalendarComponent } from '@root/modules/calendar/calendar.component';
import { ThemeConstants } from '@root/shared/config/theme-constant';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from '@root/components/components.module';

import { ChartsModule } from 'ng2-charts';

import 'd3';
import 'nvd3';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { CalendarModule } from 'ap-angular2-fullcalendar';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CalendarRoutingModule,
    RouterModule,
    ComponentsModule,
    PerfectScrollbarModule,
    ChartsModule,
    CalendarModule
  ],
  declarations: [
    CalendarComponent
  ],
  providers: [
    ThemeConstants
  ]
})
export class CalendarFeatureModule {
}
