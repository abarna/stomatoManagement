import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ThemeConstants } from '@root/shared/config/theme-constant';

import { SetAppTitleAction } from '@root/core/core.actions';

import { AppState } from '@root/store/app.state';
import { Store } from '@ngrx/store';
import { RouterModule } from '@angular/router';

@Component({
  templateUrl: 'calendar.component.html'
})

export class CalendarComponent implements OnInit, OnDestroy {

  constructor(private colorConfig: ThemeConstants,
              private formBuilder: FormBuilder,
              private appStore: Store<AppState>,
              public router: RouterModule) {
  }

  date: Date = new Date();
  d: any = new Date().getDate();
  m: any = new Date().getMonth();
  y: any = new Date().getFullYear();
  events: any;

  addEvent = function(calendarOptions) {
    calendarOptions.events.push({
      title: 'Untitle Event',
      start: new Date(this.y, this.m, 28),
      end: new Date(this.y, this.m, 29),
      desc: 'Meetings',
      className: ['openSesame']
    });
  };

  removeEvent = function(index, calendarOptions) {
    calendarOptions.events.splice(index,1);
  };

  calendarOptions:Object = {
    height: 800,
    header:{
      left: 'month,agendaWeek,agendaDay',
      center: 'title',
      right: 'today prev,next'
    },
    fixedWeekCount : false,
    editable: true,
    eventLimit: true,
    events: [
      {
        title: 'Omusoru Eduard',
        start: new Date(this.y, this.m, 1),
        desc:'Extractie',
        bullet: 'warning'
      },
      {
        title: 'Omusoru Eduard',
        start: new Date(this.y, this.m, this.d - 5),
        end: new Date(this.y, this.m, this.d - 2),
        desc:'Consultatie',
        bullet: 'success'
      },
      {
        title: 'John Doe',
        start: new Date(this.y, this.m, this.d - 3, 16, 0),
        allDay: false,
        desc:'Igienizare',
        bullet: 'warning'
      },
      {
        title: 'Jane Doe',
        start: new Date(this.y, this.m, this.d + 4, 16, 0),
        allDay: false,
        desc:'Plomba',
        bullet: 'danger'
      },
      {
        title: 'Lorem Ipsum',
        start: new Date(this.y, this.m, this.d + 1, 19, 0),
        end: new Date(this.y, this.m, this.d + 1, 22, 30),
        allDay: false,
        desc:'Plombita'
      },
      {
        title: 'Stuf sutff',
        start: new Date(this.y, this.m, 28),
        end: new Date(this.y, this.m, 29),
        desc:'Igienizare',
        bullet: 'success'
      }
    ]
  };

  themeColors = this.colorConfig.get().colors;

  ngOnInit() {
    this.appStore.dispatch(new SetAppTitleAction({title: 'Calendar'}));
  }

  ngOnDestroy() {
    this.appStore.dispatch(new SetAppTitleAction({title: null}))
  }

  onSubmit(event) {

  }
}
