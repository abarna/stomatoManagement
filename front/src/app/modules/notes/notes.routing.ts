import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { NotesComponent } from '@root/modules/notes/notes.component';

export const notesRoutes: Routes = [
  {
    path: '',
    component: NotesComponent
  }
];


@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forChild( notesRoutes )]
})
export class NotesRoutingModule {}
