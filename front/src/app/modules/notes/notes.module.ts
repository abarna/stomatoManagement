import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ThemeConstants } from '@root/shared/config/theme-constant';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from '@root/components/components.module';

import { ChartsModule } from 'ng2-charts';

import 'd3';
import 'nvd3';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NotesComponent } from '@root/modules/notes/notes.component';
import { NotesRoutingModule } from '@root/modules/notes/notes.routing';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ComponentsModule,
    PerfectScrollbarModule,
    ChartsModule,
    NotesRoutingModule
  ],
  declarations: [
    NotesComponent
  ],
  providers: [
    ThemeConstants
  ]
})
export class NotesModule {
}
