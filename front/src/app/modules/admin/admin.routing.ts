import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { AdminComponent } from '@root/modules/admin/admin.component';

export const adminRoutes: Routes = [
  {
    path: '',
    component: AdminComponent
  }
];


@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forChild( adminRoutes )]
})
export class AdminRoutingModule {}
