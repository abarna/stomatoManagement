import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { PageNotFoundRoutingModule } from '@root/modules/page-not-found/page-not-found.routing';
import { PageNotFoundComponent } from '@root/modules/page-not-found/page-not-found.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PageNotFoundRoutingModule
  ],
  declarations: [
    PageNotFoundComponent
  ],
  providers: []
})
export class PageNotFoundModule {}
