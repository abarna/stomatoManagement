import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ThemeConstants } from '@root/shared/config/theme-constant';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from '@root/components/components.module';

import { ChartsModule } from 'ng2-charts';

import 'd3';
import 'nvd3';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { AppointmentsComponent } from "@root/modules/appointments/appointments.component";
import { AppointmentsRoutingModule } from "@root/modules/appointments/appointments.routing";

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ComponentsModule,
    PerfectScrollbarModule,
    ChartsModule,
    AppointmentsRoutingModule
  ],
  declarations: [
    AppointmentsComponent
  ],
  providers: [
    ThemeConstants
  ]
})
export class AppointmentsModule {
}
