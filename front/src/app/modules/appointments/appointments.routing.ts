import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { AppointmentsComponent } from '@root/modules/appointments/appointments.component';

export const appointmentsRoutes: Routes = [
  {
    path: '',
    component: AppointmentsComponent
  }
];


@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forChild( appointmentsRoutes )]
})
export class AppointmentsRoutingModule {}
