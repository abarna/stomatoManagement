import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

import { AuthService } from '@root/modules/login/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    let token = this.authService.getToken();

    if (token) {
      const newReq = req.clone(
        {
          headers: req.headers
            .set('Token', this.authService.getToken())
        });

      return next.handle(newReq);
    }

    return next.handle(req);
  }
}

export class CacheInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const newReq = req.clone(
      {
        headers: req.headers
          .set('Pragma', 'no-cache')
          .set('Expires', '0')
          .set('Cache-Control', 'no-cache, no-store, must-revalidate')
      });

    return next.handle(newReq);
  }
}

export class CORSInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const newReq = req.clone(
      {
        headers: req.headers
          .set('Access-Control-Allow-Origin', '*')
          .set('Access-Control-Allow-Origin', 'GET, POST, PATCH, PUT, DELETE, OPTIONS')
          .set('Access-Control-Allow-Origin', 'Origin, Content-Type, X-Auth-Toke')
      });

    return next.handle(newReq);
  }
}
