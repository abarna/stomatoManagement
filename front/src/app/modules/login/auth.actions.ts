import { Action } from '@ngrx/store';

export class AuthRehydrateAction implements Action {
  public type: string = 'AUTH_REHYDRATE';

  constructor(public payload: { isLoggedIn: boolean; token?: string }) {
  }
}

export class LoginAction implements Action {
  public type: string = 'LOGIN';

  constructor(public payload: { username: string; password: string }) {
  }
}

export class LoginSuccessAction implements Action {
  public type: string = 'LOGIN_SUCCESS';

  constructor(public payload?: { token?: string, customerId?: string }) {
  }
}

export class LoginErrorAction implements Action {
  public type: string = 'LOGIN_ERROR';

  constructor(public payload?: any) {
  }
}

export class LoginRedirectAction implements Action {
  public type: string = 'LOGIN_REDIRECT';

  constructor(public payload?: { url?: string }) {
  }
}

export class LogoutAction implements Action {
  public type: string = 'LOGOUT';

  constructor(public payload?: any) {
  }
}

export class ResetAuthAction implements Action {
  public type: string = 'RESET_AUTH';

  constructor(public payload?: any) {
  }
}
