import { Store, Action, createReducer } from 'ngrx-actions';
import {
  AuthRehydrateAction,
  LoginErrorAction,
  LoginSuccessAction,
  LogoutAction,
  LoginRedirectAction,
  ResetAuthAction
} from '@root/modules/login/auth.actions';

export interface AuthState {
  isLoggedIn: boolean;
  token?: string;
  error?: string;
  loading: boolean;
}

export const initialAuthState: AuthState = {
  isLoggedIn: false,
  token: undefined,
  error: undefined,
  loading: false
};

@Store(initialAuthState)
export class AuthStore {
  @Action(AuthRehydrateAction)
  rehydrate(state: AuthState, action: AuthRehydrateAction) {
    state.isLoggedIn = action.payload.isLoggedIn;
    state.token = action.payload.token;
  }

  @Action(LoginErrorAction)
  loginError(state: AuthState, action: LoginErrorAction) {
    state.isLoggedIn = false;
    state.loading = false;
    state.error = action.payload.error;
  }

  @Action(LoginSuccessAction)
  loginSuccess(state: AuthState, action: LoginSuccessAction) {
    state.isLoggedIn = true;
    state.token = action.payload.token;
    state.loading = false;
    state.error = undefined;
  }

  @Action(LogoutAction)
  logout(state: AuthState, action: LogoutAction) {
    state.isLoggedIn = false;
    state.token = undefined;
  }

  @Action(LoginRedirectAction)
  loginRedirect(state: AuthState, action: LoginRedirectAction) {
    state.isLoggedIn = false;
    state.error = undefined;
  }

  @Action(ResetAuthAction)
  resetAuth(state: AuthState, action: ResetAuthAction) {
    state = { ...initialAuthState, ...{} };
  }
}

export function authReducer(state, action) {
  return createReducer(AuthStore)(state, action);
}
