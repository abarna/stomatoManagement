import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LoginComponent } from '@root/modules/login/login.component';

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forChild([
    {
      path: '',
      component: LoginComponent
    }
  ] )]
})
export class LoginRoutingModule {}
