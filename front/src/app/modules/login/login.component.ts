import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { AppState } from '@root/store/app.state';
import { Store } from '@ngrx/store';
import { SetAppTitleAction } from '@root/core/core.actions';
import { Router } from '@angular/router';
import { AuthState } from '@root/modules/login/auth.store';
import { LoginAction } from '@root/modules/login/auth.actions';

@Component({
  templateUrl: 'login.html'
})

export class LoginComponent implements OnInit{
  public loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private appStore: Store<AppState>,
    private authStore: Store<AuthState>) {

    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit(){
    this.appStore.dispatch(new SetAppTitleAction({title: 'Login'}));
  }

  submit() {
    const username: string = this.loginForm.get('username').value.trim();
    const password: string = this.loginForm.get('password').value.trim();

    const payload = {
      password: password,
      username: username
    };

    console.log(payload);

    this.authStore.dispatch(new LoginAction(payload));
  }
}
