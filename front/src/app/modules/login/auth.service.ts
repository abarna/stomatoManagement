import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { environment } from '@env/environment';
import { HttpClient } from '@angular/common/http';
import { Enpoints } from '@root/shared/config/endpoints';

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) {
  }

  public login(username: string, password: string): Observable<any> {
    return this.http
      .post<any>(environment.APIURL + Enpoints.LOGIN, {
        username: username,
        password: password
      })
      .map((response: any) => {
        return response.authenticateResult;
      })
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  public isLoggedIn(): boolean {
    let token: string = window.sessionStorage.getItem('token');
    return token !== null && token !== undefined;
  }

  public setToken(token: string): void {
    window.sessionStorage.setItem('token', token);
  }

  public getToken(): string {
    return window.sessionStorage.getItem('token');
  }

  public logout(): void {
    window.sessionStorage.removeItem('token');
  }
}
