import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Action } from '@ngrx/store';
import { Actions, Effect, toPayload } from '@ngrx/effects';

import { environment } from '@env/environment';

import { AuthService } from '@root/modules/login/auth.service';
import {
  LoginErrorAction,
  LoginSuccessAction
} from '@root/modules/login/auth.actions';

@Injectable()
export class AuthEffects {

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router
  ) {}

  @Effect()
  login$: Observable<Action> = this.actions$
    .ofType('LOGIN')
    //.map( action  => action.payload)
    .map( toPayload )
    .switchMap( credentials => {
      return this.authService.login( credentials.username, credentials.password )
        .map( response => new LoginSuccessAction({ token: response.token }))
        .catch( error => {
          let errorText: string = '';
          if( error.status )
            switch( error.status ){
              case 0: errorText = 'Cannot connect to the server. Please try again later.'; break;
              case 401: errorText = 'You are unauthorized to make this operation'; break;
              case 404: errorText = 'Cannot perform this operation'; break;
              case 409: errorText = 'Invalid credentials. Please try again.'; break;
              default: errorText = error.json().response;
            }
          return Observable.of( new LoginErrorAction({ error: errorText }))
        });
    });

  @Effect({ dispatch: false })
  loginSuccess$ = this.actions$
    .ofType('LOGIN_SUCCESS')
    .map( toPayload )
    .do( payload => {
      console.log(payload);
      this.authService.setToken( payload.token );
      this.router.navigate([ environment.defaultRoute ]);
    });

  @Effect({ dispatch: false })
  public logout$: Observable<Action> = this.actions$
    .ofType('LOGOUT')
    .do(() => {
      this.router.navigate(['/login']);
      this.authService.logout();
    });

  @Effect({ dispatch: false })
  loginRedirect$ = this.actions$
    .ofType( 'LOGIN_REDIRECT' )
    .map( toPayload )
    .do( payload => {
      //console.log('redirect url', payload);
      this.router.navigate(['/login']);
    });

}
