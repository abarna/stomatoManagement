import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Store } from '@ngrx/store';

import { AuthState } from '@root/modules/login/auth.store';
import { Select } from 'ngrx-actions';
import { LoginRedirectAction } from '@root/modules/login/auth.actions';
import { AuthService } from '@root/modules/login/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  @Select('auth.isLoggedIn') isLoggedIn$: Observable<boolean>;

  constructor(
    private authService: AuthService,
    private authStore: Store<AuthState>,
    private router: Router
  ){
  }

  canActivate(): Observable<boolean> {
    return this.isLoggedIn$.take(1).map( authenticated => {
      if (!authenticated) {
        this.authStore.dispatch( new LoginRedirectAction({ url: this.router.url }));
        return false;
      }

      return true;
    });
  }
}
