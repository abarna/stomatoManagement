import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoginRoutingModule } from '@root/modules/login/login.routing';
import { LoginComponent } from '@root/modules/login/login.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from '@root/modules/login/auth.effects';
import { authReducer } from '@root/modules/login/auth.store';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    LoginRoutingModule,

    StoreModule.forFeature( 'auth', authReducer ),
    EffectsModule.forFeature([ AuthEffects ])
  ],
  declarations: [
    LoginComponent
  ],
  providers: []
})
export class LoginModule {}
