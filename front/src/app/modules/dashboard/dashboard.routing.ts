import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from '@root/modules/dashboard/dashboard.component';
import { NgModule } from '@angular/core';

export const dashboardRoutes: Routes = [
  {
    path: '',
    component: DashboardComponent
  }
];


@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forChild( dashboardRoutes )]
})
export class DashboardRoutingModule {}
