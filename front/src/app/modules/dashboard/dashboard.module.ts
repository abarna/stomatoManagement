import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ThemeConstants } from '@root/shared/config/theme-constant';
import { DashboardRoutingModule } from '@root/modules/dashboard/dashboard.routing';
import { DashboardComponent } from '@root/modules/dashboard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from '@root/components/components.module';

import { ChartsModule } from 'ng2-charts';

import 'd3';
import 'nvd3';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    DashboardRoutingModule,
    RouterModule,
    ComponentsModule,
    PerfectScrollbarModule,
    ChartsModule
  ],
  declarations: [
    DashboardComponent
  ],
  providers: [
    ThemeConstants
  ]
})
export class DashboardModule {
}
