import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ThemeConstants } from '@root/shared/config/theme-constant';

import { ChartsModule } from 'ng2-charts';

import 'ammap3';
import 'ammap3/ammap/maps/js/usaLow';
import 'assets/js/jquery.sparkline/jquery.sparkline.js';
import * as $ from 'jquery';

import { SetAppTitleAction } from '@root/core/core.actions';

import { AppState } from '@root/store/app.state';
import { Store } from '@ngrx/store';
import { RouterModule } from '@angular/router';

@Component({
  templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit, OnDestroy, AfterViewInit {
  public testForm: FormGroup;
  public todaysConsults: Array<any> = [];

  constructor(private colorConfig: ThemeConstants,
              private formBuilder: FormBuilder,
              private appStore: Store<AppState>,
              public router: RouterModule) {

    this.testForm = this.formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    });
    this.todaysConsults = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  }

  themeColors = this.colorConfig.get().colors;

  //Line Chart Config
  public lineChartLabels:Array<any> = ['Dec', 'Jan', 'Feb'];
  public lineChartData:Array<any> = [
    {data: [1500, 3000, 1000], label: '2016'},
    {data: [3000, 5000, 6000], label: '2017'},
    {data: [1080, 3800, 4000], label: '2018'}
  ];
  public lineChartOptions:any = {
    scales: {
      yAxes: [
        {
          id: 'y-axis-1',
          type: 'linear',
          display: true,
          position: 'left'
        },
        {
          id: 'y-axis-2',
          type: 'linear',
          display: false
        },
        {
          id: 'y-axis-3',
          type: 'linear',
          display: false
        }
      ]
    }
  };
  public lineChartLegend:boolean = false;
  public lineChartType:string = 'line';
  public lineChartColors:Array<any> = [
    {
      backgroundColor: this.themeColors.infoInverse,
      borderColor: this.themeColors.info
    },
    {
      backgroundColor: this.themeColors.successInverse,
      borderColor: this.themeColors.success
    }
  ];

  ngOnInit() {
    this.appStore.dispatch(new SetAppTitleAction({title: 'Dashboard'}));
  }

  ngAfterViewInit(){

    var sparklineBarData1 = [100, 80, 280, 1000, 500, 499, 330, 800 ,1200, 1000];

    (<any>$("#bar-option-2")).sparkline(sparklineBarData1,
      {
        type: 'bar',
        height: '100',
        barWidth: 22,
        barSpacing: 5,
        barColor: this.themeColors.success
      });
  }

  ngOnDestroy() {
    this.appStore.dispatch(new SetAppTitleAction({title: null}))
  }

  onSubmit(event) {

  }
}
