import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { MaterialsComponent } from '@root/modules/materials/materials.component';
import { RequiredMaterialsComponent } from '@root/modules/materials/required-materials/required-materials.component';
import { EquipmentsComponent } from '@root/modules/materials/equipments/equipments.component';

export const materialRoutes: Routes = [
  {
    path: '',
    component: MaterialsComponent,
    children: [
    {
      path: 'required',
      component: RequiredMaterialsComponent
    },
    {
      path: 'equipments',
      component: EquipmentsComponent
    }]
  }
];


@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forChild( materialRoutes )]
})
export class MaterialsFeatureRoutingModule {}
