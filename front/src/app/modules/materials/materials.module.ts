import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ThemeConstants } from '@root/shared/config/theme-constant';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from '@root/components/components.module';

import { ChartsModule } from 'ng2-charts';

import 'd3';
import 'nvd3';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { MaterialsFeatureRoutingModule } from '@root/modules/materials/materials.routing';
import { MaterialsComponent } from '@root/modules/materials/materials.component';
import { RequiredMaterialsComponent } from './required-materials/required-materials.component';
import { EquipmentsComponent } from './equipments/equipments.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ComponentsModule,
    PerfectScrollbarModule,
    ChartsModule,
    MaterialsFeatureRoutingModule
  ],
  declarations: [
    MaterialsComponent,
    RequiredMaterialsComponent,
    EquipmentsComponent
  ],
  providers: [
    ThemeConstants
  ]
})
export class MaterialsModule {
}
