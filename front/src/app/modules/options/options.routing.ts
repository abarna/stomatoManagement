import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { OptionsComponent } from '@root/modules/options/options.component';

export const optionsRoutes: Routes = [
  {
    path: '',
    component: OptionsComponent
  }
];


@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forChild( optionsRoutes )]
})
export class OptionsRoutingModule {}
