package com.docApp.util.converters;

import com.docApp.entities.UserEntity;
import com.docApp.entities.security.RoleEntity;
import com.docApp.util.users.UserVO;
import org.junit.Test;

import static com.docApp.Randomizer.aRandomString;
import static com.docApp.entities.builders.RoleEntityBuilder.aRoleEntity;
import static com.docApp.entities.builders.UsersEntityBuilder.anUsersEntity;
import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;

public class UserEntityToUserVOConverterTest {

    private UserEntityToUserVOConverter converter = new UserEntityToUserVOConverter();

    @Test
    public void convert() {
        RoleEntity adminRole = aRoleEntity().withRole("ADMIN").build();
        RoleEntity userRole = aRoleEntity().withRole("USER").build();
        String password = aRandomString();
        UserEntity userEntity = anUsersEntity()
                .withEncryptedPassword(password)
                .withPassword(password)
                .withId(3)
                .withName(aRandomString())
                .withRoles(newArrayList(adminRole,userRole))
                .withUsername(aRandomString())
                .withValidated(true)
                .build();

        UserVO userVO = converter.convert(userEntity);

        assertThat(userVO).isEqualToIgnoringGivenFields(userEntity, "roles");
        assertThat(userVO.getRoles()).hasSize(2);
        assertThat(userVO.getRoles().get(0)).isEqualTo(adminRole.getRole());
        assertThat(userVO.getRoles().get(1)).isEqualTo(userRole.getRole());
    }
}