package com.docApp.repository;

import com.docApp.config.IntegrationDatasourceConfiguration;
import com.docApp.entities.UserEntity;
import com.docApp.entities.builders.UsersEntityBuilder;
import com.docApp.repositories.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import static com.docApp.Randomizer.aRandomString;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Import(IntegrationDatasourceConfiguration.class)
public class UserRepositoryIT {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Test
    public void testFindByName() {
        UserEntity userEntity = UsersEntityBuilder.anUsersEntity()
                .withEncryptedPassword(aRandomString())
                .withName(aRandomString())
                .withUsername(aRandomString()).build();
        em.persist(userEntity);
        UserEntity expectedUserEntity = userRepository.findByUsername(userEntity.getUsername());
        assertThat(userEntity).isEqualToIgnoringGivenFields(expectedUserEntity, "roles");
    }
}