package com.docApp.controllers;

import com.docApp.config.BaseMvcTest;
import com.docApp.facades.api.UserFacade;
import com.docApp.util.users.UserVO;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.web.WebAppConfiguration;

import static com.docApp.Randomizer.aRandomString;
import static com.docApp.util.users.builder.UserVOBuilder.anUserVO;
import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration()
@AutoConfigureMockMvc
@SpringBootTest
public class UserControllerTest extends BaseMvcTest {

    @Mock
    private UserFacade facade;
    @InjectMocks
    private UserController userController;

    @After
    public void tearDown() {
        verifyNoMoreInteractions(facade);
    }

    private ArgumentCaptor<UserVO> userVOArgumentCaptor = ArgumentCaptor.forClass(UserVO.class);

    @Test
    @WithMockUser
    public void getAllUsers() throws Exception {
        UserVO anUser = anUserVO().withName(aRandomString()).build();
        when(facade.getUsers()).thenReturn(newArrayList(anUser));
        mockMvc.perform(get(getTestedUrl("getAllUsers")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is(anUser.getName())));

        verify(facade).getUsers();
    }

    @Test
    public void createAccount() throws Exception {
        UserVO anUser = anUserVO().withName("Adi").withUsername(aRandomString()).withEncryptedPassword(aRandomString()).build();
        String requestJson = getJsonFromObject(anUser);
        mockMvc.perform(post(getTestedUrl("createAccount"))
                .contentType(APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isOk());

        verify(facade).createAccount(userVOArgumentCaptor.capture());

        assertThat(userVOArgumentCaptor.getValue()).isEqualToComparingFieldByField(anUser);
    }

    @Override
    protected Object getTestedController() {
        return userController;
    }

    @Override
    protected String getControllerBaseUrl() {
        return "/users";
    }
}