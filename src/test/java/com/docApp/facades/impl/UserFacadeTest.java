package com.docApp.facades.impl;

import com.docApp.facades.api.UserFacade;
import com.docApp.services.api.UserService;
import com.docApp.util.users.UserVO;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static com.docApp.Randomizer.aRandomString;
import static com.docApp.util.users.builder.UserVOBuilder.anUserVO;
import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserFacadeTest {

    @InjectMocks
    private UserFacade userFacade = new UserFacadeImpl();

    @Mock
    private UserService userService;

    @After
    public void tearDown() {
        verifyNoMoreInteractions(userService);
    }


    @Test
    public void getUsers() {

        List<UserVO> expectedResult = newArrayList(anUserVO().withUsername(aRandomString()).build());
        when(userService.getUsers()).thenReturn(expectedResult);
        List<UserVO> users = userFacade.getUsers();

        assertThat(users).isEqualTo(expectedResult);
        verify(userService).getUsers();
    }
}
