package com.docApp.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.docApp.util.users.UserVO;
import org.junit.Before;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public abstract class BaseMvcTest {

    protected MockMvc mockMvc;

    @Before
    public void init() {

        mockMvc = MockMvcBuilders.standaloneSetup(getTestedController()).build();
    }

    protected abstract Object getTestedController();
    protected abstract String getControllerBaseUrl();

    protected String getJsonFromObject(UserVO anUser) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(anUser);
    }

    protected String getTestedUrl(String parameterWithoutControllerBaseUrl){
        return getControllerBaseUrl() + "/" + parameterWithoutControllerBaseUrl;
    }
}
