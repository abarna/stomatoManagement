package com.docApp.services;

import com.docApp.entities.UserEntity;
import com.docApp.exceptions.users.UsernameNotAvailableException;
import com.docApp.repositories.UserRepository;
import com.docApp.services.impl.UserServiceImpl;
import com.docApp.util.converters.UserEntityToUserVOConverter;
import com.docApp.util.users.UserVO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import java.util.List;

import static com.docApp.Randomizer.aRandomString;
import static com.docApp.entities.builders.UsersEntityBuilder.anUsersEntity;
import static com.docApp.util.users.builder.UserVOBuilder.anUserVO;
import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private UserEntityToUserVOConverter userConverter;

    @Mock
    private UserRepository userRepository;

    @Mock
    private EntityManager entityManager;

    @InjectMocks
    private UserServiceImpl userService;

    private UserVO userVO;
    private UserEntity userEntity;

    @Before
    public void setUp() {
        userVO = anUserVO()
                .withName(aRandomString())
                .withUsername(aRandomString())
                .withEncryptedPassword(aRandomString())
                .build();
        userEntity = anUsersEntity()
                .withName(aRandomString())
                .withUsername(aRandomString())
                .withEncryptedPassword(aRandomString())
                .build();
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(userConverter, userRepository, entityManager);
    }

    @Test
    public void getUsers() {
        List<UserEntity> usersEntities = newArrayList(userEntity);
        when(userRepository.findAll()).thenReturn(usersEntities);
        when(userConverter.convert(userEntity)).thenReturn(userVO);
        List<UserVO> users = userService.getUsers();

        assertThat(users).hasSize(1);
        verify(userConverter).convert(userEntity);
        verify(userRepository).findAll();
    }

    @Test
    public void saveUser() {
        when(userRepository.findByUsername(userVO.getUsername())).thenReturn(null);
        when(userConverter.convertToEntity(userVO)).thenReturn(userEntity);
        userService.saveUser(userVO);

        verify(userRepository).findByUsername(userVO.getUsername());
        verify(userConverter).convertToEntity(userVO);
        verify(entityManager).persist(userEntity);
        //TODO remove it when account validation process is implemented
        assertThat(userEntity.isValidated()).isTrue();

    }

    @Test
    public void saveUser_ThrowExceptionWhenUsernameNotAvailable() {
        when(userRepository.findByUsername(userVO.getUsername())).thenReturn(userEntity);
        Throwable throwable = catchThrowable(() -> userService.saveUser(userVO));

        verify(userRepository).findByUsername(userVO.getUsername());
        assertThat(throwable).isInstanceOf(UsernameNotAvailableException.class);
        assertThat(throwable.getMessage()).isEqualTo("The username " + userVO.getUsername() + " is not available");
    }

    @Test
    public void addRoleToUser() {
        when(userRepository.findOne(userVO.getId())).thenReturn(userEntity);

    }
}