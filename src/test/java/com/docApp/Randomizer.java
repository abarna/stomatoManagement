package com.docApp;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;

import static java.time.Month.JANUARY;
import static java.time.ZoneOffset.UTC;

public class Randomizer {
    private static Random rnd = new Random();

    public Randomizer() {
    }

    public static String aRandomStringOfLength(int len) {
        StringBuilder sb = new StringBuilder(len);

        for (int i = 0; i < len; ++i) {
            sb.append("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(rnd.nextInt("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".length())));
        }

        return sb.toString();
    }

    public static String aRandomString() {
        return aRandomStringOfLength(20);
    }

    public static String aRandomStringOfMaxLength(int maxlen) {
        return aRandomStringOfLength(aRandomInteger(maxlen - 1) + 1);
    }

    public static String aRandomNumericStringOfLength(int len) {
        StringBuilder sb = new StringBuilder(len);

        for (int i = 0; i < len; ++i) {
            sb.append("0123456789".charAt(rnd.nextInt("0123456789".length())));
        }

        return sb.toString();
    }

    public static String aRandomNumericStringOfMaxLength(int maxlen) {
        return aRandomNumericStringOfLength(aRandomInteger(maxlen - 1) + 1);
    }

    public static Integer aRandomInteger() {
        return rnd.nextInt();
    }

    public static Integer aRandomInteger(int n) {
        return rnd.nextInt(n);
    }

    public static Integer aRandomInteger(int min, int max) {
        return rnd.ints(min, max + 1).findFirst().getAsInt();
    }

    public static String aRandomE164() {
        return "+" + aRandomNumericStringOfLength(12);
    }

    public static Long aRandomLong() {
        return rnd.nextLong();
    }

    public static BigDecimal aRandomBigDecimal() {
        return BigDecimal.valueOf(rnd.nextLong());
    }

    public static BigDecimal aRandomBigDecimal(int max) {
        return BigDecimal.valueOf((long) aRandomInteger(max));
    }

    public static Boolean aRandomBoolean() {
        return rnd.nextBoolean();
    }

    public static String aRandomEmail() {
        return aRandomStringOfLength(10) + "@" + aRandomStringOfLength(5) + ".test";
    }

    public static String aRandomUri() {
        return aRandomStringOfLength(6) + "@" + aRandomStringOfLength(5) + ".test";
    }

    public static String aRandomUriOfLength(int length) {
        if (length < 13) {
            throw new IllegalArgumentException("The length must be 13 or grater.");
        } else {
            int userNameLength = 6;
            String domain = ".test";
            return aRandomStringOfLength(userNameLength) + "@" + aRandomStringOfLength(length - (userNameLength + "@".length() + domain.length())) + domain;
        }
    }

    public static String aRandomUrl() {
        return aRandomStringOfLength(3) + "." + aRandomStringOfLength(6) + ".test";
    }

    public static LocalDate aRandomLocalDate() {
        LocalDate start = LocalDate.of(1970, JANUARY, 1);
        long days = ChronoUnit.DAYS.between(start, LocalDate.now().plusYears(10L));
        return start.plusDays((long) rnd.nextInt((int) days + 1));
    }

    public static OffsetDateTime aRandomUtcDateTime() {
        return aRandomLocalDate().atStartOfDay().atOffset(UTC);
    }

    public static LocalDate aRandomLocalDateInTheFuture() {
        LocalDate start = LocalDate.now();
        long days = ChronoUnit.DAYS.between(start, start.plusYears(10L));
        return start.plusDays((long) rnd.nextInt((int) days + 1));
    }

    public static LocalDate aRandomLocalDateInThePast() {
        LocalDate start = LocalDate.of(1970, JANUARY, 1);
        long days = ChronoUnit.DAYS.between(start, LocalDate.now());
        return LocalDate.now().minusDays((long) rnd.nextInt((int) days + 1));
    }
}