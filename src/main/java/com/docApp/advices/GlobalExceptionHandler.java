package com.docApp.advices;

import com.docApp.exceptions.api.ApiError;
import com.docApp.exceptions.users.UsernameNotAvailableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import static org.springframework.http.HttpStatus.*;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(NOT_FOUND)
    public ApiError noHandlerFoundException(
            NoHandlerFoundException ex) {

        int code = 1000;
        String message = "No handler found for your request.";
        return new ApiError(code, message);
    }

    @ExceptionHandler(UsernameNotAvailableException.class)
    @ResponseStatus(BAD_REQUEST)
    public ApiError usernameNotAvailableException(
            UsernameNotAvailableException ex) {

        int code = 1001;
        String message = ex.getMessage();
        return new ApiError(code, message);
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public ApiError genericException(
            Throwable ex) {

        int code = 1002;
        String message = ex.getMessage();
        return new ApiError(code, message);
    }
}