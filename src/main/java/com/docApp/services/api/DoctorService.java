package com.docApp.services.api;

import java.util.List;

public interface DoctorService {

    String getDoctorByName(String name);

    String getDoctorById(Integer id);

    List<String> getDoctors();
}
