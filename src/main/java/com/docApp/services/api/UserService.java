package com.docApp.services.api;

import com.docApp.entities.UserEntity;
import com.docApp.util.users.UserVO;

import java.util.List;

public interface UserService {

    List<UserVO> getUsers();

    void updateUser(UserVO userVO);

    UserEntity getUserByUsername(String name);

    void addRoleToUser(Integer userId, String role );

    void saveUser(UserVO userVO);
}
