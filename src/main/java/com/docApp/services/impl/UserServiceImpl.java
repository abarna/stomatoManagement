package com.docApp.services.impl;

import com.docApp.entities.UserEntity;
import com.docApp.entities.security.RoleEntity;
import com.docApp.exceptions.users.InvalidRoleException;
import com.docApp.exceptions.users.UserAlreadyHasRoleException;
import com.docApp.exceptions.users.UsernameNotAvailableException;
import com.docApp.repositories.RoleRepository;
import com.docApp.repositories.UserRepository;
import com.docApp.services.api.UserService;
import com.docApp.util.converters.UserEntityToUserVOConverter;
import com.docApp.util.users.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.StreamSupport;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private UserEntityToUserVOConverter converter;

    @Override
    public List<UserVO> getUsers() {
        Iterable<UserEntity> usersIterator = userRepository.findAll();
        return StreamSupport.stream(usersIterator.spliterator(), false)
                .map(converter::convert)
                .collect(toList());
    }

    @Override
    public UserEntity getUserByUsername(String name) {
        return userRepository.findByUsername(name);
    }

    @Override
    public void updateUser(UserVO userVO) {
        UserEntity userEntity = converter.convertToEntity(userVO);
        entityManager.merge(userEntity);
    }


    @Override
    public void addRoleToUser(Integer userId, String role) {
        UserEntity userEntity = userRepository.findOne(userId);
        ifUserAlreadyHasRoleThrowException(role, userEntity);
        RoleEntity roleEntityForBeingAdded = roleRepository.findByRole(role);
        if (isNull(roleEntityForBeingAdded)) {
            throw new InvalidRoleException("The role " + role + "is invalid!");
        }
        roleEntityForBeingAdded.setRole(role);
        userEntity.getRoles().add(roleEntityForBeingAdded);
    }

    private void ifUserAlreadyHasRoleThrowException(String role, UserEntity userEntity) {
        userEntity.getRoles().stream().filter(roleEntity -> roleEntity.getRole().equals(role)).findFirst().ifPresent(roleEntity -> {
            throw new UserAlreadyHasRoleException("The user already has the role: " + role);
        });
    }

    @Override
    public void saveUser(UserVO userVO) {
        UserEntity userEntityFromDB = userRepository.findByUsername(userVO.getUsername());

        if (!isNull(userEntityFromDB)) {
            throw new UsernameNotAvailableException("The username " + userVO.getUsername() + " is not available");
        }

        UserEntity userEntity = converter.convertToEntity(userVO);
        //TODO remove it when implemented account validation
        userEntity.setValidated(true);
        entityManager.persist(userEntity);
    }
}
