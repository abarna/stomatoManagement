package com.docApp.services.impl;

import com.docApp.entities.DoctorEntity;
import com.docApp.repositories.DoctorRepository;
import com.docApp.services.api.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.hibernate.validator.internal.util.CollectionHelper.newArrayList;

@Service
public class DoctorServiceImpl implements DoctorService {

    @Autowired
    private DoctorRepository doctorRepository;

    @Override
    public String getDoctorByName(String name) {
        DoctorEntity doctorEntity = doctorRepository.findDoctorsEntityByName(name);

        return "Id: " + doctorEntity.getId() +
                " Name: " + doctorEntity.getName();
    }

    @Override
    public String getDoctorById(Integer id) {
        DoctorEntity doctorEntity = doctorRepository.findDoctorsEntityById(id);
        return "Id: " + doctorEntity.getId() +
                " Name: " + doctorEntity.getName();
    }

    @Override
    public List<String> getDoctors() {
        List<String> result = newArrayList();
        Iterable<DoctorEntity> doctorIterator = doctorRepository.findAll();
        doctorIterator.forEach(doctor -> result.add(doctor.getName()));
        return result;
    }
}
