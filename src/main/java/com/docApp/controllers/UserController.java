package com.docApp.controllers;

import com.docApp.facades.api.UserFacade;
import com.docApp.security.JwtTokenUtil;
import com.docApp.security.JwtUser;
import com.docApp.util.users.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserFacade facade;

    @Value("Authorization")
    private String tokenHeader;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;

    @RequestMapping(value = "/getAllUsers", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserVO> getAllUsers() {
        return facade.getUsers();
    }

    @RequestMapping(value = "/addRole/{userId}/{role}", method = PUT)
    public void addRole(@PathVariable("userId") Integer userId, @PathVariable("role") String role) {
        facade.addRoleToUser(userId, role);
    }

    @RequestMapping(value = "/createAccount", method = POST)
    public void createAccount(@RequestBody UserVO userVO) {
        facade.createAccount(userVO);
    }

    @RequestMapping(value = "/protected", method = GET)
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getProtectedGreeting() {
        return ResponseEntity.ok("Greetings from admin protected method!");
    }

    @RequestMapping(value = "user", method = GET)
    public JwtUser getAuthenticatedUser(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader).substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);
        return user;
    }
}
