package com.docApp.controllers;

import com.docApp.facades.impl.DoctorFacadeImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/doctors")
public class DoctorController {
    @Autowired
    private DoctorFacadeImpl doctorsFacade;

    @RequestMapping(value = "/findDoctorByName/{name}", method = GET)
    public String getDoctorByName(@PathVariable("name") String name) {
        return doctorsFacade.getDoctorByName(name);
    }

    @RequestMapping(value = "/findDoctorById/{id}", method = GET)
    public String getDoctorById(@PathVariable("id") Integer id) {
        return doctorsFacade.getDoctorById(id);
    }

    @RequestMapping(value = "/getAllDoctors", method = GET)
    public List<String> getAllDoctors() {
        return doctorsFacade.getDoctors();
    }
}
