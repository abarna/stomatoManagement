package com.docApp.repositories;

import com.docApp.entities.DoctorEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorRepository extends CrudRepository<DoctorEntity, Integer> {

    DoctorEntity findDoctorsEntityByName(String name);

    DoctorEntity findDoctorsEntityById(Integer id);
}
