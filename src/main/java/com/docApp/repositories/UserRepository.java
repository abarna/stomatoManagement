package com.docApp.repositories;

import com.docApp.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Integer> {

    //TODO what happens when multiple records are returned?
    UserEntity findByUsername(String name);

}
