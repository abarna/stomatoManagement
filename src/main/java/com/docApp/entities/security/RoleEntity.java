package com.docApp.entities.security;

import com.docApp.entities.UserEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

import static org.hibernate.validator.internal.util.CollectionHelper.newArrayList;

@Entity(name = "role")
public class RoleEntity {

    @Id
    @GeneratedValue
    private Integer id;
    //TODO change it to RoleEnum
    private String role;

    @ManyToMany(mappedBy = "roles")
    private List<UserEntity> users = newArrayList();

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(List<UserEntity> users) {
        this.users = users;
    }

    public void addUser(UserEntity user) {
        if (!this.users.contains(user)) {
            this.users.add(user);
        }

        if (!user.getRoles().contains(this)) {
            user.getRoles().add(this);
        }
    }

    public void removeUser(UserEntity user) {
        this.users.remove(user);
        user.getRoles().remove(this);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
