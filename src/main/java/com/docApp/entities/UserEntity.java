package com.docApp.entities;

import com.docApp.entities.security.RoleEntity;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;

@Entity(name = "users")
public class UserEntity {

    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private String username;

    @Transient
    private String password;

    private String encryptedPassword;
    private boolean validated;
    @ManyToMany(fetch = EAGER, cascade = ALL)
    @JoinTable
    // ~ defaults to @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "users_id"),
    //     inverseJoinColumns = @joinColumn(name = "roles_id"))
    private List<RoleEntity> roles;

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public List<RoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleEntity> roles) {
        this.roles = roles;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }
}
