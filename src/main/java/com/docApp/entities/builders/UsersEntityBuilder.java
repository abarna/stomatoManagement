package com.docApp.entities.builders;

import com.docApp.entities.UserEntity;
import com.docApp.entities.security.RoleEntity;

import java.util.List;

public final class UsersEntityBuilder {
    private Integer id;
    private String name;
    private String username;
    private String password;
    private String encryptedPassword;
    private boolean validated;
    // ~ defaults to @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "users_id"),
    //     inverseJoinColumns = @joinColumn(name = "roles_id"))
    private List<RoleEntity> roles;

    private UsersEntityBuilder() {
    }

    public static UsersEntityBuilder anUsersEntity() {
        return new UsersEntityBuilder();
    }

    public UsersEntityBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public UsersEntityBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public UsersEntityBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    public UsersEntityBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public UsersEntityBuilder withEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
        return this;
    }

    public UsersEntityBuilder withValidated(boolean validated) {
        this.validated = validated;
        return this;
    }

    public UsersEntityBuilder withRoles(List<RoleEntity> roles) {
        this.roles = roles;
        return this;
    }

    public UserEntity build() {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(id);
        userEntity.setName(name);
        userEntity.setUsername(username);
        userEntity.setPassword(password);
        userEntity.setEncryptedPassword(encryptedPassword);
        userEntity.setValidated(validated);
        userEntity.setRoles(roles);
        return userEntity;
    }
}
