package com.docApp.entities.builders;

import com.docApp.entities.UserEntity;
import com.docApp.entities.security.RoleEntity;

import java.util.List;

import static org.hibernate.validator.internal.util.CollectionHelper.newArrayList;

public final class RoleEntityBuilder {
    private Integer id;
    //TODO change it to RoleEnum
    private String role;
    private List<UserEntity> users = newArrayList();

    private RoleEntityBuilder() {
    }

    public static RoleEntityBuilder aRoleEntity() {
        return new RoleEntityBuilder();
    }

    public RoleEntityBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public RoleEntityBuilder withRole(String role) {
        this.role = role;
        return this;
    }

    public RoleEntityBuilder withUsers(List<UserEntity> users) {
        this.users = users;
        return this;
    }

    public RoleEntity build() {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setId(id);
        roleEntity.setRole(role);
        roleEntity.setUsers(users);
        return roleEntity;
    }
}
