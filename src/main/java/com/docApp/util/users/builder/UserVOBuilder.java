package com.docApp.util.users.builder;

import com.docApp.util.users.UserVO;

import java.util.List;

public final class UserVOBuilder {
    private Integer id;
    private String name;
    private String username;
    private String encryptedPassword;
    private boolean validated;
    private List<String> roles;

    private UserVOBuilder() {
    }

    public static UserVOBuilder anUserVO() {
        return new UserVOBuilder();
    }

    public UserVOBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public UserVOBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public UserVOBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    public UserVOBuilder withEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
        return this;
    }

    public UserVOBuilder withValidated(boolean validated) {
        this.validated = validated;
        return this;
    }

    public UserVOBuilder withRoles(List<String> roles) {
        this.roles = roles;
        return this;
    }

    public UserVO build() {
        UserVO userVO = new UserVO();
        userVO.setId(id);
        userVO.setName(name);
        userVO.setUsername(username);
        userVO.setPassword(encryptedPassword);
        userVO.setValidated(validated);
        userVO.setRoles(roles);
        return userVO;
    }
}
