package com.docApp.util.enums.security;

import java.util.Arrays;

public enum RolesEnum {
    USER("USER"),
    ADMIN("ADMIN"),
    NA("NA");


    RolesEnum(String name) {
        this.name = name;
    }

    private String name;

    public RolesEnum getValueOf(String value) {
        return Arrays.stream(RolesEnum.values())
                .filter(rolesEnum -> rolesEnum.name.equalsIgnoreCase(value))
                .findFirst()
                .orElse(NA);
    }

}
