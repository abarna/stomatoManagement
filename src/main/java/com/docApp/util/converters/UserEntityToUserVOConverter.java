package com.docApp.util.converters;

import com.docApp.entities.UserEntity;
import com.docApp.entities.security.RoleEntity;
import com.docApp.util.users.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import static com.docApp.entities.builders.UsersEntityBuilder.anUsersEntity;
import static com.docApp.util.users.builder.UserVOBuilder.anUserVO;
import static java.util.stream.Collectors.toList;

@Component
public class UserEntityToUserVOConverter implements Converter<UserEntity, UserVO> {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserVO convert(UserEntity userEntity) {
        return anUserVO()
                .withId(userEntity.getId())
                .withEncryptedPassword(userEntity.getEncryptedPassword())
                .withName(userEntity.getName())
                .withUsername(userEntity.getUsername())
                .withValidated(userEntity.isValidated())
                .withRoles(userEntity.getRoles().stream()
                        .map(RoleEntity::getRole)
                        .collect(toList()))
                .build();
    }

    public UserEntity convertToEntity(UserVO userVO) {
        return anUsersEntity()
                .withId(userVO.getId())
                .withEncryptedPassword(passwordEncoder.encode(userVO.getPassword()))
                .withName(userVO.getName())
                .withUsername(userVO.getUsername())
                .withValidated(userVO.isValidated())
                .build();
    }
}
