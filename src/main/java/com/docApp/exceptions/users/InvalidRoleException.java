package com.docApp.exceptions.users;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ResponseStatus(BAD_REQUEST)
public class InvalidRoleException extends RuntimeException {

    public InvalidRoleException(String errorMessage){
        super(errorMessage);
    }
}
