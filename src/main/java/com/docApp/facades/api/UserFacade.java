package com.docApp.facades.api;

import com.docApp.util.users.UserVO;

import java.util.List;

public interface UserFacade {

    List<UserVO> getUsers();

    void addRoleToUser(Integer userId, String role);

    void createAccount(UserVO user);
}
