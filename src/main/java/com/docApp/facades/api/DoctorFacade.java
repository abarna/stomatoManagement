package com.docApp.facades.api;

import java.util.List;

public interface DoctorFacade {

    String getDoctorByName(String name);

    String getDoctorById(Integer id);

    List<String> getDoctors();
}
