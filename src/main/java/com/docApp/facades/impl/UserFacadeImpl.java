package com.docApp.facades.impl;

import com.docApp.facades.api.UserFacade;
import com.docApp.services.api.UserService;
import com.docApp.util.users.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
public class UserFacadeImpl implements UserFacade {
    @Autowired
    private UserService service;

    @Override
    public List<UserVO> getUsers() {
        return service.getUsers();
    }

    @Override
    @Transactional
    public void addRoleToUser(Integer userId, String role) {
        service.addRoleToUser(userId, role);
    }

    @Override
    @Transactional
    public void createAccount(UserVO user) {
        service.saveUser(user);
    }
}
