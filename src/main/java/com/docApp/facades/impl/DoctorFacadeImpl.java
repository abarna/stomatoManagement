package com.docApp.facades.impl;

import com.docApp.facades.api.DoctorFacade;
import com.docApp.services.api.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DoctorFacadeImpl implements DoctorFacade {
    @Autowired
    private DoctorService doctorsService;

    @Override
    public String getDoctorByName(String name) {
        return doctorsService.getDoctorByName(name);
    }

    @Override
    public String getDoctorById(Integer id) {
        return doctorsService.getDoctorById(id);
    }

    @Override
    public List<String> getDoctors() {
        return doctorsService.getDoctors();
    }

}
