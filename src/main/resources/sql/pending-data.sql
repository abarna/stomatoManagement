use docApp;
INSERT INTO role (id, role) VALUES (1, 'ADMIN_ROLE');
INSERT INTO users (id, username, name, encrypted_password, validated) VALUES (1, 'abarna', 'adrian', '$2a$08$UkVvwpULis18S19S5pZFn.YHPZt3oaqHZnDwqbCW9pft6uFtkXKDC', 1);
INSERT INTO users (id, username, name, encrypted_password, validated) VALUES (2, 'simona', 'simona', '$2a$10$qS4I26CsoCYfZXDEsmSCn.Hjtlgv.emOL4c07WcVnbcgTfDpXeY3.', 1);
INSERT INTO users (id, username, name, encrypted_password, validated) VALUES (3, 'test', 'test', '$2a$08$UkVvwpULis18S19S5pZFn.YHPZt3oaqHZnDwqbCW9pft6uFtkXKDC', 1);
INSERT INTO users_roles (users_id, roles_id) VALUES (1, 1);