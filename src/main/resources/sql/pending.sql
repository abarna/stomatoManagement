DROP DATABASE stomato;
CREATE DATABASE docApp;
USE docApp;
create table doctors
(
	id int auto_increment
		primary key,
	name varchar(30) not null
)
;

create table role
(
	id int auto_increment
		primary key,
	role varchar(20) null,
	constraint role_role_uindex
		unique (role)
)
;

create table users
(
	id int auto_increment
		primary key,
	username varchar(30) not null,
	name varchar(20) not null,
	encrypted_password varchar(250) not null,
	validated tinyint default '0' not null,
	constraint users_username_uindex
		unique (username)
)
;

create table users_roles
(
	users_id int null,
	roles_id int null,
	constraint users_roles_users_id_roles_id_pk
		unique (users_id, roles_id),
	constraint users_fk
		foreign key (users_id) references users (id),
	constraint roles_fk
		foreign key (roles_id) references role (id)
)
;

create index roles_fk
	on users_roles (roles_id)
;

